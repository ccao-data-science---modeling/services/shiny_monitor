#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

  CREATE TABLE IF NOT EXISTS pins (
    id serial PRIMARY KEY,
    pin character(14),
    pin_valid boolean,
    pinval_version text,
    pinval_user text,
    entered_at timestamp
  );

  CREATE INDEX ON pins (pin);

EOSQL
